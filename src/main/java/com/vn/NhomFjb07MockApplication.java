package com.vn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NhomFjb07MockApplication {

    public static void main(String[] args) {
        SpringApplication.run(NhomFjb07MockApplication.class, args);
    }

}
